# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 

## Scaling up

Split them into different microservices to scope down the requirements of each domain Cuisine and Customer, and a third service to store the relationship between them. Even though this is a graph relationship, would bet on a storage like Cassandra, using cuisine name as partition key and clustering on user id, or the opposite depending on the average use case, and a view allowing queries in the opposite direction. Another alternative is to use Kafka, not only as message bus but as storage, consolidating the relationships in KTables, there is a few pros and cons on both alternatives.
