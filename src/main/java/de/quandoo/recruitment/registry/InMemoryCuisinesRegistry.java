package de.quandoo.recruitment.registry;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Multimap<String, String> cuisineCustomers = HashMultimap.create();

    private Multimap<String, String> customerCuisines = HashMultimap.create();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        Objects.requireNonNull(customer);
        Objects.requireNonNull(customer.getUuid());
        Objects.requireNonNull(cuisine);
        Objects.requireNonNull(cuisine.getName());

        customerCuisines.put(customer.getUuid(), cuisine.getName());
        cuisineCustomers.put(cuisine.getName(), customer.getUuid());
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return Optional
            .ofNullable(cuisine)
            .map(Cuisine::getName)
            .map(customerCuisines::get)
            .orElse(Collections.emptyList())
            .stream()
            .map(Customer::new)
            .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        return Optional
            .ofNullable(customer)
            .map(Customer::getUuid)
            .map(cuisineCustomers::get)
            .orElse(Collections.emptyList())
            .stream()
            .map(Cuisine::new)
            .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        return cuisineCustomers.asMap()
            .entrySet()
            .stream()
            .sorted(
                Map.Entry.comparingByValue(
                    Comparator.<Collection<String>, Integer>comparing(Collection::size).reversed()
                )
            )
            .limit(n)
            .map(Map.Entry::getKey)
            .map(Cuisine::new)
            .collect(Collectors.toList());
    }
}
