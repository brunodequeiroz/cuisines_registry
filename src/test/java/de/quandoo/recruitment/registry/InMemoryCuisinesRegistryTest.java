package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry;

    @Before
    public void setUp() {
        cuisinesRegistry = new InMemoryCuisinesRegistry();
    }

    @Test
    public void shouldWork1() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
    }

    @Test
    public void shouldWork2() {
        cuisinesRegistry.cuisineCustomers(null);
    }

    @Test
    public void shouldWork3() {
        cuisinesRegistry.customerCuisines(null);
    }

    @Test
    public void thisDoesntWorkYet() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("5"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("6"), new Cuisine("german"));

        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(3);

        assertThat(cuisines.get(0).getName(), is("italian"));
        assertThat(cuisines.get(1).getName(), is("french"));
        assertThat(cuisines.get(2).getName(), is("german"));
    }
}
